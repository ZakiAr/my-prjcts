//: Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"

func example(p1: Int, p2: Int) -> Int{
    return p1 + p2
}

example(p1: 10, p2:5)
example(p1: 4, p2: 2)