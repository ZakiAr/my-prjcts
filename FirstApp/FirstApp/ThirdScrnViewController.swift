//
//  ThirdScrnViewController.swift
//  FirstApp
//
//  Created by Harlie  Brindak on 1/27/17.
//  Copyright © 2017 Harlie  Brindak. All rights reserved.
//

import UIKit

class ThirdScrnViewController: UIViewController, UITableViewDataSource{
    
    //set the number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    //set the number of rows per section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 5
        }else{
            return 5
        }
        
    }
    
    //set the cells content
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = "you!!!"
        cell.detailTextLabel?.text = "haha!! :P"
        
        let img = UIImage(named: "icony")
        cell.imageView?.image = img
        
        return cell
    }
    
    //set title for sections
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "me me"
    }
    
    
    
}
