 //
//  ViewController.swift
//  FirstApp
//
//  Created by Harlie  Brindak on 1/16/17.
//  Copyright © 2017 Harlie  Brindak. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // picker view code
    let nameArr = ["you", "him", "her"]
    var started : Bool = false
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return nameArr.count
    }
    // onPickerViewCreated method
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if started == false{
            self.view.backgroundColor = UIColor.magenta
            txtfield.text = nameArr[row]
            started = true}
        return nameArr[row]
    }
    // onItemSelected method
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtfield.text = nameArr[row]
        self.view.backgroundColor = UIColor.blue
        txtfield.backgroundColor = UIColor.yellow
        
    }
    
    // loading the view
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // buttons and txtfields test
    @IBOutlet weak var txtfield: UITextField!
    
    @IBOutlet weak var lblfield: UILabel!
    
    @IBAction func changetext(_ sender: Any) {
        lblfield.text = "hi " + txtfield.text!
        self.txtfield.resignFirstResponder()
    }
    // keyboard hiding code
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // memory management code
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

