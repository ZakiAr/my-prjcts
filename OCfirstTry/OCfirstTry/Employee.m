//
//  Employee.m
//  OCfirstTry
//
//  Created by Harlie  Brindak on 1/19/17.
//  Copyright © 2017 Harlie  Brindak. All rights reserved.
//

#import "Employee.h"

@implementation Employee
- (void)dealloc
{
    //ARC does it for you
    NSLog(@"check it");
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _messageC = @"me init";
        NSLog(@"%@", _messageC);
    }
    return self;
}

-(void) methodOne{
    NSLog(@"hi from the class");
}

@end
