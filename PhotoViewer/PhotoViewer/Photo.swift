//
//  Photo.swift
//  PhotoViewer
//
//  Created by Harlie  Brindak on 1/30/17.
//  Copyright © 2017 Harlie  Brindak. All rights reserved.
//

import Foundation

struct Photo {
    
    var name : String
    var filename: String
    var notes : String
    
}
